let list = document.querySelector('.tabs')

list.addEventListener('click' , event => {
    let delClass = document.querySelector('.active')
    if(delClass){
        delClass.classList.remove('active')
    }
    event.target.classList.add('active')
    let text = document.getElementById(event.target.innerHTML)
    let remActive = document.querySelector('.tabs-text-active')
    if(remActive){
        remActive.classList.remove('tabs-text-active')
    }
    if(text){
        text.classList.add('tabs-text-active')
    }
})